# BridSDK IOS Example

This SDK aims at easily playing videos with or without ads on your IOS application. Its was written in objective c, and support swift and objective c programming language.   
Example depends of GoogleAds-IMA-iOS-SDK, so it was puted trought pods.

* Dependency: GoogleAds-IMA-iOS-SDK

## Example application

The integration of the Brid SDK is very simple. 

BridSDK gives you the factory methods to pair the player with your player id that you set up in BRID CMS.
Each Player (BVPlayer) is created with the help of your credentials (which includes id player, playlist id.. namely ID of player configuration).
BVPlayer places itself within the Container view you have at your disposal, which is basically a UIView, iits on you to:


1) Setup a BVObject data object where you can configure a player instance using your credentials


2) Give him a container view in which to display them

Container view (UIView) by itself will retain the @weak reference of BVPlayer view but not BVPlayer itself, which in addition to the view contains logic and netwokring, 
so the player will not work unless stored as a @strong reference.

#### Example 1:

If you have one container view that is not reusable, create one ``var player = BVPlayer (…)``, and give player.setView (containerView)


#### Example 2: 

If you have reusable viewers (UITableView, UICollectionView), create a array ``var player: [BVPlayer]`` in which you save the players and in each preparation of a new reusable cell (UITableCell) re-adjust the view from the player list

The stop, pause, and other commands are executed on the BVPlayer object.

## You can choose type of BVData:

### Player with sigle video

Swift:``BVData(playerID: Int32, forVideoID: Int32)`` 

Objc:``[[BVData alloc] initPlayerID:<int> forVideoID:<int>]``

### Player with with playlist

Swift:``BVData(playerID: Int32, forPlaylistID: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forPlaylistID:<int>]``

### Player with channel playlist and option from what page and how many to load


Swift:``BVData(playerID: Int32, forChanneltID: Int32, page: Int32, item: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forChanneltID:<int> page:<int> item:<int>]``

### Player with latest playlist and option from what page and how many to load

Swift:``BVData(playerID: Int32, forLatestID: Int32, page: Int32, item: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forLatestID:<int> page:<int> item:<int>]``

### Player with tag playlist, it loads video with tag string and option from what page and how many to load

Swift:``BVData(playerID: Int32, forTag: String, page: Int32, item: Int32)``

Objc:``[[BVData alloc] initPlayerID:<int> forTag:<String> page:<int> item:<int>]``




---

# How to use

### Declare a player variable:

### Swift:  
``var player: BVPlayer?``

### Objc:  
``@property (nonatomic, strong) BVPlayer player;``  

### Initialize player vith object:

### Swift:  
``player = BVPlayer(data: BVData(playerID: <Int32>, forVideoID: <Int32>), for: <UIView>)``

### Objc:  
``player = [[BVPlayer alloc] initWithData:[[BVData alloc] initPlayerID:<int> forVideoID:<int>]  forView:<UIView>]``

### Listen to evnets from player:

You can listen to events of player true Notification Centre. There is player events and ad events. Just change the string in notification.usreInfo.
 
### Swift: 
```
func setupEventNetworking() {  
       NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "PlayerEvent"), object: nil) 
	   NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "AdEvent"), object: nil) 
}
@objc func eventWriter(_ notification: NSNotification) {       
	if notification.name.rawValue == "PlayerEvent" 
		print(notification.userInfo?["event"] as! String)
   	}  
 	if notification.name.rawValue == "AdEvent" {   
		print(notification.userInfo?["ad"] as! String)
    }   
}
```

### Objc:
```
- (void)setupEventNetworking {   
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventWriter:) name:@"PlayerEvent" object:nil];   
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eventWriter:) name:@"AdEvent" object:nil];    
}  
- (void) eventWriter:(NSNotification *)notification {
	if ([notification.name isEqualToString:@"PlayerEvent"]) {  
    	[self logMessage:(NSString *)notification.userInfo[@"event"]]; 
	}  
	if ([notification.name isEqualToString:@"AdEvent"]) { 
    	[self logMessage:(NSString *)notification.userInfo[@"ad"]];  
	}  
}  
```

## BridSDK player example methods:

### Swift:	 
``player.play()`` 					
``player.pause()`` 				
``player.mute()`` 					
``player.unmute()``               
``player.next()``                     
``player.previous()``                                         
``player.stop()``                                  

### Objc:	  
``[self.player play];`` 			
``[self.player pause];`` 			
``[self.player mute];`` 			
``[self.player unmute];`` 		     
``[self.player next];`` 		     
``[self.player previous];`` 		     
``[self.player stop];`` 			     

## BridSDK player getter methods:

### Swift:  
``player.isFullscreen()``			
``player.isPlayerInView()`` 					
``player.getCurrentTime()`` 				
``player.getDuration()`` 					
``player.getMuted()``               
``player.getVolume()``                     
``player.isAdInProgress()``                        
``player.getCurrentIndex()``               
``player.getPlaylist()``                     
``player.getSource()``            
``player.getVideo()``        

### Objc:  
``[self.player isFullscreen];`` 	
``[self.player isPlayerInView];`` 			
``[self.player getCurrentTime];`` 			
``[self.player getDuration];`` 			
``[self.player getMuted];`` 		     
``[self.player getVolume];`` 		     
``[self.player isAdInProgress];`` 		    	
``[self.player getCurrentIndex];`` 		     
``[self.player getPlaylist];`` 		     
``[self.player getSource];`` 		    			     
``[self.player getVideo];`` 		    			     


## BridSDK player setters methods:

### Swift:  
``player?.setVolume(<volume: Float>)``			
``player.play(by: <Int>)``			
``player.setVideo(<video: VideoData>)`` 					
``player.setVideoUrl(<url: String>)`` 				
``player.setPlaylist(<url: URL>)`` 					
``player.setAd(<ad: [AdData]>)``               
``player.playAd(<ad: AdData>)``                     
``player.playAdTagUrl(<url: URL>)``                        
``player.setAdMacros(<macros: AdMacros>)``               
``player.autoHideControls(<isHidden: Bool>)``                          

### Objc:  
``[self.player setVolume:<volume: Float>];`` 	
``[self.player playByIndex:<Int>];`` 			
``[self.player setVideo:<video: VideoData>];`` 			
``[self.player setVideoUrl:<url: String>];`` 			
``[self.player setPlaylist:<url: URL>];`` 		     
``[self.player setAd:<ad: [AdData]>];`` 		     
``[self.player playAd:<ad: AdData>];`` 		    	
``[self.player playAdTagUrl:<url: URL>];`` 		     
``[self.player setAdMacros:<macros: AdMacros>];`` 		     
``[self.player autoHideControls:<isHidden: Bool>];`` 		    			      	


---

## Requirements

	•	XCode10 or newer
	•	iOS 11.0+
	•	CocoaPods


