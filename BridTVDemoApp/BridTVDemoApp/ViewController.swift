//
//  ViewController.swift
//  BridTVDemoApp
//
//  Created by Xcode Peca on 07/02/2020.
//  Copyright © 2020 Xcode Peca. All rights reserved.
//

import UIKit
import BridSDK

class ViewController: UIViewController {
    
    @IBOutlet weak var ctnView: UIView!
    
    var player: BVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        player = BVPlayer(data: BVData(playerID: 18527, forPlaylistID: 7572), for: ctnView)
    }
    
    @IBAction func methodsTapped(_ sender: Any) {
        //        player?.play()
        //        player?.previous()
        //        player?.next()
        //        player?.pause()
        //        player?.mute()
        //        player?.unmute()
        //        player?.stop()
    }
    
    @IBAction func getSetTapped(_ sender: Any) {
        var consleText = ""
        
        //        consleText = "\(String(describing: player?.getCurrentTime()))"
        //        consleText =  "\(String(describing: player?.getDuration()))"
        //        consleText =  "\(String(describing: player?.getMuted()))"
        //        consleText =  "\(String(describing: player?.getVolume()))"
        //        consleText =  "\(String(describing: player?.isAdInProgress()))"
        //        consleText =  "\(String(describing: player?.getCurrentIndex()))"
        //        consleText =  "\(String(describing: player?.getSource()))"
        //        consleText =  "\(String(describing: player?.getPlaylist()))"
        //        consleText =  "\(String(describing: player?.isFullscreen()))"
        
        print(consleText)
        
        //        player?.setVolume(<#T##volume: Float##Float#>)
        //        player?.play(by: <#T##Int#>)
        //        player?.setVideo(<#T##video: VideoData?##VideoData?#>)
        //        player?.setVideoUrl(<#T##url: String?##String?#>)
        //        player?.setPlaylist(<#T##url: URL?##URL?#>)
        //        player?.setAd(<#T##ad: [AdData]?##[AdData]?#>)
        //        player?.playAd(<#T##ad: AdData?##AdData?#>)
        //        player?.playAdTagUrl(<#T##url: URL?##URL?#>)
        //        player?.setAdMacros(<#T##macros: AdMacros?##AdMacros?#>)
        //        player?.autoHideControls(<#T##isHidden: Bool##Bool#>)
    }
    
    func setupEventNetworking() {
        NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "PlayerEvent"), object: nil)
    }
    
    func setupEventNetworkingForAd() {
        NotificationCenter.default.addObserver(self, selector: #selector(eventWriter), name:NSNotification.Name(rawValue: "AdEvent"), object: nil)
    }
    
    @objc func eventWriter(_ notification: NSNotification) {
        if notification.name.rawValue == "PlayerEvent" {
          print(notification.userInfo?["event"] as! String)
        }
        
        if notification.name.rawValue == "AdEvent" {
             print(notification.userInfo?["ad"] as! String)
        }
    }
    
}

