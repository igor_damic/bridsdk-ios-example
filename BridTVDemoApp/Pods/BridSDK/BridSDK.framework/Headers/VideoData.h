//
//  VideoData.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/14/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Source.h"
#import "Tracks.h"
#import "Parser.h"

@interface VideoData : Parser

@property (nonatomic, strong, nullable) NSString *_id;
@property (nonatomic, strong, nullable) NSString *duration;
@property (nonatomic, strong, nullable) NSString *ageGateId;
@property (nonatomic, strong, nullable) NSString *name;
@property (nonatomic, strong, nullable) NSString *_description;
@property (nonatomic, strong, nullable) NSString *image;
@property (nonatomic, strong, nullable) NSString *thumbnail;
@property (nonatomic, strong, nullable) NSString *clickthroughUrl;
@property (nonatomic, strong, nullable) NSString *thumb;
@property (nonatomic, strong, nullable) NSDate *publish;
@property (nonatomic, strong, nullable) NSString *mimeType;
@property (nonatomic, strong, nullable) NSString *webp;   //Stavljen je string, ali ne znam sta stize
@property (nonatomic, assign) BOOL monetize;
@property (nonatomic, assign) BOOL liveimage;
@property (nonatomic, assign) BOOL secureUrl;
@property (nonatomic, strong, nullable) Source *source;
@property (nonatomic, strong, nullable) NSArray<Tracks *> *tracks;

- (instancetype _Nullable )initWithVideo:(NSString *_Nullable)_id
                     duration:(NSString *_Nullable)duration
                  ageGateId:(NSString *_Nullable)ageGateId
                         name:(NSString *_Nullable)name
                 _description:(NSString *_Nullable)_description
                        image:(NSString *_Nullable)image
                    thumbnail:(NSString *_Nullable)thumbnail
              clickthroughUrl:(NSString *_Nullable)clickthroughUrl
                        thumb:(NSString *_Nullable)thumb
                      publish:(NSDate *_Nullable)publish
                     mimeType:(NSString *_Nullable)mimeType
                         webp:(NSString *_Nullable)webp
                     monetize:(BOOL *_Nullable)monetize
                    liveimage:(BOOL *_Nullable)liveimage
                    secureUrl:(BOOL *_Nullable)secureUrl
                       source:(Source *_Nullable)source
                       tracks:(NSArray<Tracks *> *_Nullable)tracks;

@end
