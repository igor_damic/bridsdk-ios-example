//
//  BVMobileData.h
//  BridSDK
//
//  Created by Predrag Jevtic on 10/4/19.
//  Copyright © 2019 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BVMobileData : NSObject

@property (strong, nonatomic, readonly, nullable) NSString *idUrl;
@property (strong, nonatomic, readonly, nullable) NSString *adURL;

- (instancetype _Nonnull)initWithID:(NSString *_Nullable)idUrl url:(NSString *_Nullable)adURL;

@end
