//
//  BVAdData.h
//  BridSDK
//
//  Created by Marko on 4/15/19.
//  Copyright © 2019 Brid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BVMobileData.h"

@interface BVAdData : BVMobileData

@property (strong, nonatomic, readonly, nullable) NSArray<BVMobileData *> *mobileDataArray;


- (instancetype _Nonnull)initURLs:(NSArray<BVMobileData *> *_Nullable)mobileDataArray forRollType:(float)type;
- (float)rollType;

@end
