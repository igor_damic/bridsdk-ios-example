//
//  Parser.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/18/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Parser : NSObject

@property (nonatomic, readonly, strong, nullable) NSDictionary *asDictionary;

- (instancetype)initWithJSON:(NSDictionary *) parser;
+ (id)createDataObject:(NSDictionary *)json forName:(NSString *)name;

@end
