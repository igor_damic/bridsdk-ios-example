//
//  ConfigurationData.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/18/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"
#import "PlayerData.h"
#import "PartnerData.h"
#import "VideoData.h"
#import "AdData.h"
#import "ChannelData.h"
#import "PlaylistData.h"
#import "ErrorData.h"
#import "AdMacros.h"

@interface ConfigurationData : Parser

@property (nonatomic, readonly, strong) PlayerData *Player;
@property (nonatomic, readonly, strong) PartnerData *Patner;
@property (nonatomic, readonly, strong) NSArray<VideoData *> *Video;
@property (nonatomic, strong) NSArray<AdData *> *Ad;
@property (nonatomic, readonly, strong) ChannelData *Channel;
@property (nonatomic, readonly, strong) PlaylistData *Playlist;
@property (nonatomic, readonly, strong) ErrorData *Error;


- (instancetype)initWithJSON:(NSDictionary*)parser;
- (instancetype) initWithPlayer:(PlayerData*)player
                         patner:(PartnerData*)patner
                          video:(NSArray<VideoData*>*)video
                             ad:(NSArray<AdData*>*)ad
                        channel:(ChannelData*)channel
                       playlist:(PlaylistData*)playlist
                          error:(ErrorData*)error;

@end
