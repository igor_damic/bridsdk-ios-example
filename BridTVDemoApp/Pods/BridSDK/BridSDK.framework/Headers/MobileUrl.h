//
//  MobileUrl.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/14/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mobile.h"
#import "Parser.h"

@interface MobileUrl : Parser

@property (nonatomic, readonly, strong, nullable) NSArray<Mobile *> *mobile;

@end
