//
//  AdData.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/8/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mobile.h"
#import "Parser.h"

typedef enum : NSUInteger {
    Preroll,
    Midroll,
    Postroll,
    Overlay,
    Banner
} AdType;

@interface AdData : Parser

@property (nonatomic, strong, nullable) NSString *adType;
@property (nonatomic, strong, nullable) NSString *overlayStartAt;
@property (nonatomic, strong, nullable) NSString *overlayDuration;
@property (nonatomic, strong, nullable) NSString *adTimeType;
@property (nonatomic, strong, nullable) NSString *cuepoints;
@property (nonatomic, strong, nullable) NSArray<Mobile *> *mobile;

extern const int TYPE_PROGRAM;
extern const int TYPE_PARTNER;
extern const int TYPE_PREROLL;
extern const int TYPE_MIDROLL;
extern const int TYPE_POSTROLL;

- (instancetype _Nullable )initWithAdType:(NSString *_Nullable)adType
                       overlayStartAt:(NSString *_Nullable)overlayStartAt
                      overlayDuration:(NSString *_Nullable)overlayDuration
                           adTimeType:(NSString *_Nullable)adTimeType
                            cuepoints:(NSString *_Nullable)cuepoints
                               mobile:(NSArray<Mobile *> *_Nullable)mobile;
@end
