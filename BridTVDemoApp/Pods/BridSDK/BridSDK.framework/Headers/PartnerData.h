//
//  PartnerData.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/12/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UrchinFields.h"
#import "Program.h"
#import "Parser.h"

@interface PartnerData : Parser

@property (nonatomic, readonly, strong, nullable) NSString *_id;
@property (nonatomic, readonly, strong, nullable) NSString *user_id;
@property (nonatomic, readonly, strong, nullable) NSString *maxBufferLength;
@property (nonatomic, readonly, strong, nullable) NSString *maxBufferSize;
@property (nonatomic, readonly, strong, nullable) NSString *urchin;
@property (nonatomic, readonly, strong, nullable) NSString *domain_short;
@property (nonatomic, readonly, strong, nullable) NSString *_override;
@property (nonatomic, readonly, strong, nullable) NSString *bridAdOffset;
@property (nonatomic, readonly, assign) BOOL deblocker;
@property (nonatomic, readonly, assign) BOOL brand;
@property (nonatomic, readonly, assign) BOOL payed;
@property (nonatomic, readonly, assign) BOOL bridAdPartner;
@property (nonatomic, readonly, assign) BOOL upload;
@property (nonatomic, strong, nullable) UrchinFields *urchin_fields;
@property (nonatomic, readonly, strong, nullable) Program *program;
@property (nonatomic, readonly, strong, nullable) NSArray *ads;
@end
