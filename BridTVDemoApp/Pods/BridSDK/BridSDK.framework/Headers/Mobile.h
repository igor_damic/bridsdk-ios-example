//
//  Mobile.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/14/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"


@interface Mobile : Parser

@property (nonatomic, readonly, strong, nullable) NSString *_id;
@property (nonatomic, readonly, strong, nullable) NSString *url;

- (instancetype _Nullable )initWithMobile:(NSString *_Nullable)_id
                                 url:(NSString *_Nullable)url;

@end
