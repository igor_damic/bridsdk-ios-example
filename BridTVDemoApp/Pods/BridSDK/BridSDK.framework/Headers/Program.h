//
//  Program.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/14/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MobileUrl.h"
#import "Parser.h"

@interface Program : Parser

@property (nonatomic, readonly, strong, nullable) MobileUrl *mobile;

@end
