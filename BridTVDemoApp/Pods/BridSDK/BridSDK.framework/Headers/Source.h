//
//  Source.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/14/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"

@interface Source : Parser

@property (nonatomic, strong, nullable) NSString *streaming;
@property (nonatomic, strong, nullable) NSString *ld;
@property (nonatomic, strong, nullable) NSString *sd;
@property (nonatomic, strong, nullable) NSString *hsd;
@property (nonatomic, strong, nullable) NSString *hd;


//@property (nonatomic, readonly, strong, nullable) NSString *ogg;
//@property (nonatomic, readonly, strong, nullable) NSString *mp3;

@end
