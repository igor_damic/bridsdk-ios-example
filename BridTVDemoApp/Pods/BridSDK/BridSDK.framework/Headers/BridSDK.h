//
//  BridSDK.h
//  BridSDK
//
//  Created by Marko on 3/14/19.
//  Copyright © 2019 Brid. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BridSDK.
FOUNDATION_EXPORT double BridSDKVersionNumber;

//! Project version string for BridSDK.
FOUNDATION_EXPORT const unsigned char BridSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like
#import <BridSDK/BVPlayer.h>
#import <BridSDK/BVData.h>
#import <BridSDK/BVAdData.h>
#import <BridSDK/BVMobileData.h>
