//
//  Channel.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/14/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"


@interface ChannelData : Parser

@property (nonatomic, readonly, strong, nullable) NSString *name;
@property (nonatomic, readonly, strong, nullable) NSString *tag_channel;

@end
