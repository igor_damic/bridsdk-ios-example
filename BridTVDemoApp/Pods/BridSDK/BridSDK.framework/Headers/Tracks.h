//
//  TracksModel.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/14/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"


@interface Tracks : Parser

@property (nonatomic, readonly, strong, nullable) NSString *kind;
@property (nonatomic, readonly, strong, nullable) NSString *src;

@end
