//
//  Error.h
//  PlayerForIMATestObjc
//
//  Created by Predrag Jevtic on 3/25/19.
//  Copyright © 2019 Predrag Jevtic. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Parser.h"

@interface ErrorData : Parser

@property (nonatomic, readonly, strong, nullable) NSString *m;

@end
